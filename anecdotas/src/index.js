import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css';

const App = () => {

  const anecdotes = [
    {
      vote: 0, 
      anecdota: 'If it hurts, do it more often'
    },
    {
      vote: 0, 
      anecdota: 'Adding manpower to a late software project makes it later!'
    },
    {
      vote: 0, 
      anecdota:  'The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time'
    },
    {
      vote: 0, 
      anecdota: 'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.'
    },
    {
      vote: 0, 
      anecdota:  'Premature optimization is the root of all evil.'
    },
    {
      vote: 0, 
      anecdota:  'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code ascleverly as possible, you are, by definition, not smart enough to debug it.'
    }
  ]

  const [selected, setSelected] = useState(0)
  const [votes, setVotes] = useState(anecdotes)

  const datoAleatorio = () => {
    var anecdotas = anecdotes.length -1
    var anecdota = Math.floor((Math.random() * anecdotas )-1)+1
    setSelected(anecdota)
  }

  const hacerVotacion = () => {
    let votos = [...votes] 
    votos[selected].vote += 1
    setVotes(votos)
  }

  var votos = [...votes]
  let mayorAnecdota = votos.sort((a,b) => b.vote - a.vote)

  return (
    <div className="d-flex justify-content-center align-items-center pt-5" style={{ margin:'30px 70px 0px 70px' }}>
      <div className="shadow-lg p-3 mb-5 bg-light rounded text-dark" style={{ width:'50%', border:'1px solid gray'}}>
        <h1 style={{ textAlign:'center', color:'red', marginTop:'15px' }}>ANECDOTAS</h1>
        <div className="d-flex justify-content-center align-items-center">
          <button className="btn btn-outline-success" style={{ margin:'5px' }} onClick={datoAleatorio}>next anecdote</button>
          <button className="btn btn-outline-info" style={{ margin:'5px' }} onClick={hacerVotacion}>vote</button>
        </div>
        <div className="text-center mt-4">
          <p><b>ANECDOTA DEL DIA:</b> {anecdotes[selected].anecdota}</p>
          <p>Has {votes[selected].vote} votes</p>
          <p><b>ANECDOTA MAS VOTADA:</b> {mayorAnecdota[0].anecdota}</p> 
          <p><b> - Cantidad de votos:</b> {mayorAnecdota[0].vote}</p>
        </div>
      </div>
    </div>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
