import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import Statistics from './components/statistics'
import Button from './components/Button'
import 'bootstrap/dist/css/bootstrap.css';

const App = () => {

  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)

  const ClickBueno = () => {
    setGood(good + 1)
  }

  const ClickNeutral = () => {
    setNeutral(neutral + 1)
  }

  const ClickMalo = () => {
    setBad(bad + 1)
  }

  return (
    <div className="d-flex justify-content-center align-items-center pt-5" style={{ margin:'30px 70px 0px 70px'}}>
      <div className="shadow-lg p-3 mb-5 bg-light rounded text-dark" style={{ width:'50%', background:'' }}>
        <h1 style={{ textAlign:'center', color:'red', marginTop:'15px' }}>Give Feedback</h1>
        <div className="d-flex justify-content-center align-items-center">
          <Button clase="btn btn-outline-success" click={ClickBueno} text="good"/>
          <Button clase="btn btn-outline-info" click={ClickNeutral} text="neutral"/>
          <Button clase="btn btn-outline-danger" click={ClickMalo} text="bad"/>
        </div>
  
        <div className="d-flex justify-content-center align-items-center">
          {good===0 && neutral===0 && bad===0 ? (
            <p>aun no hiciste click!</p>
          ): (
            <Statistics good={good} neutral={neutral} bad={bad}/>
          )}
        </div>
      </div>

    </div>
  )
}

ReactDOM.render(<App />, 
  document.getElementById('root')
)


