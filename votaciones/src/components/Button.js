const Button = (props) => {
    return (
        <button style={{ margin:'5px' }} className={props.clase} onClick={props.click}>{props.text}</button>
    )
}

export default Button